Git global setup

git config --global user.name "Alvaro de Gracia Pastor"
git config --global user.email "alvarode1994@gmail.com"


Create a new repository
git clone https://gitlab.com/AlvarodeGracia/libgdx-android-practica.git
cd libgdx-android-practica
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master


Existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/AlvarodeGracia/libgdx-android-practica.git
git add .
git commit -m "Initial commit"
git push -u origin master


Existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/AlvarodeGracia/libgdx-android-practica.git
git push -u origin --all
git push -u origin --tags


Update Changes
git add .
git commit -m "Texto"
git push -u origin master


Create Branch
git checkout -b <nombre_branch>
git commit -am "My feature is ready"
git push origin <nombre_branch>

Borrar un branch
git push origin :<branch-name>

Merge Branch to Master
//Escogemos desde que rama trabajar
git checkout master
//Nos bajamos el repositori ode esa rama
git pull origin master
//Unimos la rama que indiquemos aqui copn la que nos hemos bajado
git merge <Branch-merge>
//Subimos los cambios
git push origin master


Actualizar el branch de la master